/*
	G2OMP - CNetwork.h
	Author(s): Osmith
	Date: 18.02.19
*/

#pragma once

class CNetwork {
public:
	CNetwork();
	~CNetwork();

	bool init();

	const char* getLocalIP() { return rn_Peer->GetLocalIP(0); }

private:
	RakNet::RakPeerInterface *rn_Peer;
};