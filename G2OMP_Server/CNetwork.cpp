/*
	G2OMP - CNetwork.cpp
	Author(s): Osmith
	Date: 18.02.19
*/

#include "stdafx.h"

//-------------------------------------------------------------------------------------------------------------------------

CNetwork::CNetwork() {};

CNetwork::~CNetwork() {};

//-------------------------------------------------------------------------------------------------------------------------

bool CNetwork::init() {
	using namespace RakNet;
	rn_Peer = RakPeerInterface::GetInstance();
	SocketDescriptor sockDesc(SERVER_PORT, 0);
	StartupResult startupResult = rn_Peer->Startup(MAX_SLOTS, &sockDesc, 1);
	bool returnResult = false;

	switch (startupResult) {
	case RAKNET_ALREADY_STARTED:
		std::cout << "# Error! Server already started!" << std::endl;
		break;
	case INVALID_SOCKET_DESCRIPTORS:
		std::cout << "# Error! Invalid socket descriptors!" << std::endl;
		break;
	case INVALID_MAX_CONNECTIONS:
		std::cout << "# Error! Invalid max connections!" << std::endl;
		break;
	case SOCKET_FAMILY_NOT_SUPPORTED:
		std::cout << "# Error! Socket family not supported!" << std::endl;
		break;
	case SOCKET_PORT_ALREADY_IN_USE:
		std::cout << "# Error! Port already in use!" << std::endl;
		break;
	case SOCKET_FAILED_TO_BIND:
		std::cout << "# Error! Socket failed to bind!" << std::endl;
		break;
	case PORT_CANNOT_BE_ZERO:
		std::cout << "# Error! Port cannot be zero!" << std::endl;
		break;
	case FAILED_TO_CREATE_NETWORK_THREAD:
		std::cout << "# Error! Failed to create network thread!" << std::endl;
		break;
	case COULD_NOT_GENERATE_GUID:
		std::cout << "# Error! Could not create GUID!" << std::endl;
		break;
	case RAKNET_STARTED:
		returnResult = true;
		break;
	default:
		std::cout << "# Error! Undefined error!" << std::endl;
		break;
	}

	if (returnResult)
		rn_Peer->SetMaximumIncomingConnections(MAX_SLOTS);

	return returnResult;
}