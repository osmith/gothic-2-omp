
/* 
	G2OMP - CServer.h
	Author(s): Osmith
	Date: 18.02.19
*/

#pragma once

class CServer {
public:
	CServer();
	~CServer();

	void init();
	bool loop();
	void cmdHandler();
	void stop();
private:

	CNetwork *Network;
	CCommandManager *CommandManager;

	bool b_Exit = false;
	bool b_cmdExecuted = true;
};