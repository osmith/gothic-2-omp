#pragma once

const double PI = 3.1416;			
const double DEGREE = 180.0 / PI;	


#include <windows.h>
#include <WinNT.h>
#include "zstring.h"


inline int zFloat2Int (const float f)
{
	int i;
	__asm fld [f]
	__asm fistp [i]
	return i;
}

inline void zClamp(int& value, int bl, int bh)
{
	if(value < bl)
		value = bl;
	else if(value > bh)
		value = bh;
}


inline void zClamp(float& value, float bl, float bh)
{
	if(value < bl)
		value = bl;
	else if(value > bh)
		value = bh;
}


class zVEC2				
{
public:
	float		n[2];
public:
	zVEC2();
	
	zVEC2(float, float);
	zVEC2& operator+=(zVEC2 const&);
	zVEC2& operator-=(zVEC2 const&);
	zVEC2& operator*=(float);
	float& operator[](int);
	const float& operator[](int) const;
	zVEC2& operator/=(float);
	
	float Length2();
	
	zVEC2& Apply(float (*)(float));
	zSTRING GetDescription() const;
	void SetByDescription(zSTRING const&);
};


inline float& zVEC2::operator[](int index)
{
	return n[index];
}


inline const float& zVEC2::operator[](int index) const
{
	return n[index];
}




float operator*(zVEC2 const&, zVEC2 const&);

zVEC2 operator+(zVEC2 const&, zVEC2 const&);

zVEC2 operator-(zVEC2 const&, zVEC2 const&);

zVEC2 operator*(zVEC2 const&, float);

zVEC2 operator/(zVEC2 const&, float);

zVEC2 operator-(zVEC2 const&);




class zVEC3				
{
public:
	float		n[3];
	zVEC3();
	zVEC3(float);
	zVEC3(float, float, float);
	zVEC3& operator-=(zVEC3 const&);
	zVEC3& operator+=(zVEC3 const&);
	zVEC3& operator*=(float);
	zVEC3& operator/=(float);
	float& operator[](int);
	const float& operator[](int) const;
	float Length2() const;
	float Length() const;
	float LengthApprox() const;
	float Distance(const zVEC3&) const;
	float DistanceAbs(const zVEC3&) const;
	zVEC3& Normalize();
	zVEC3& NormalizeSafe();
	zVEC3& NormalizeApprox();
	BOOL IsEqualEps(zVEC3 const&) const;
	
	zSTRING GetString(void)const;
	void SetByDescription( const zSTRING& str );
	zVEC3& Apply(float (*)(float));
	float MultAbs(const zVEC3&) const;
	
	float GetX();
	float GetY();
	float GetZ();
	
	float Dot(const zVEC3& other) const
	{
		const zVEC3& va = *this;
		const zVEC3& vb = other;
		return va[0] * vb[0] + va[1] * vb[1] + va[2] * vb[2];
	};

	zVEC3 Cross(const zVEC3& other) const
	{
		const zVEC3& va = *this;
		const zVEC3& vb = other;
		return zVEC3(va[1] * vb[2] - va[2] * vb[1],
					 va[2] * vb[0] - va[0] * vb[2],
					 va[0] * vb[1] - va[1] * vb[0]);
	};
};


inline float& zVEC3::operator[](int index)
{
	return n[index];
}


inline const float& zVEC3::operator[](int index) const
{
	return n[index];
}


inline float zVEC3::Length2() const
{
	return n[0] * n[0] + n[1] * n[1] + n[2] * n[2];
}


inline float zVEC3::Length() const
{
	return sqrt(Length2());
}



BOOL operator==(zVEC3 const&, zVEC3 const&);

zVEC3 operator+(zVEC3 const&, zVEC3 const&);

zVEC3 operator-(zVEC3 const&, zVEC3 const&);

zVEC3 operator^(zVEC3 const&, zVEC3 const&);

zVEC3 operator*(zVEC3 const&, float);

zVEC3 operator/(zVEC3 const&, float);

float operator*(zVEC3 const&, zVEC3 const&);

zVEC3 operator-(zVEC3 const&);



inline float operator*(zVEC3 const& va, zVEC3 const& vb)
{
	return va[0] * vb[0] + va[1] * vb[1] + va[2] * vb[2];
}



class zVEC4					
{
public:
	float		n[4];
public:
	zVEC4();
	zVEC4(float);
	zVEC4(zVEC3 const&, float);
	zVEC4(float, float, float, float);
	zVEC4& operator+=(zVEC4 const&);
	zVEC4& operator-=(zVEC4 const&);
	zVEC4& operator*=(float);
	float& operator[](int);
	const float& operator[](int) const;
	float Length2() const;
	float LengthApprox() const;
	zVEC4& Normalize();
	zVEC4& Apply(float (*)(float));
};


inline float& zVEC4::operator[](int index)
{
	return n[index];
}


inline const float& zVEC4::operator[](int index) const
{
	return n[index];
}


inline float zVEC4::Length2() const
{
	return n[0] * n[0] + n[1] * n[1] + n[2] * n[2] + n[3] * n[3];
}


BOOL operator==(zVEC4 const&, zVEC4 const&);

zVEC4 operator*(zVEC4 const&, float);

float operator*(zVEC4 const&, zVEC4 const&);

zVEC4 operator+(zVEC4 const&, zVEC4 const&);

zVEC4 operator-(zVEC4 const&, zVEC4 const&);

zVEC4 operator-(zVEC4 const&);


inline float operator*(zVEC4 const& va, zVEC4 const& vb)
{
	return va[0] * vb[0] + va[1] * vb[1] + va[2] * vb[2]  + va[3] * vb[3];
}


class zMAT3					
{

	zVEC3		row[3];

public:
	zMAT3() {};
	zMAT3(zVEC3 const&, zVEC3 const&, zVEC3 const&);
	zMAT3(float);
	zMAT3& operator+=(zMAT3 const&);
	zMAT3& operator-=(zMAT3 const&);
	zMAT3& operator*=(float);
	zMAT3& operator/=(float);
	zVEC3& operator[](int);
	const zVEC3& operator[](int) const;
	zMAT3 Transpose() const;
	zMAT3 Inverse(float*);
	void SetAtVector(zVEC3 const&);
	zVEC3 GetAtVector() const;
	void SetUpVector(zVEC3 const&);
	zVEC3 GetUpVector() const;
	void SetRightVector(zVEC3 const&);
	zVEC3 GetRightVector() const;
	void MakeOrthonormal();
	zMAT3& Apply(float (*)(float));
};


inline zVEC3& zMAT3::operator[](int index)
{
	return row[index];
}


inline const zVEC3& zMAT3::operator[](int index) const
{
	return row[index];
}


zMAT3 operator-(zMAT3 const&);

zMAT3 operator+(zMAT3 const&, zMAT3 const&);

zMAT3 operator-(zMAT3 const&, zMAT3 const&);

zMAT3 operator*(zMAT3 const&, zMAT3 const&);

zVEC3 operator*(zMAT3 const&, zVEC3 const&);

zVEC2 operator*(zMAT3 const&, zVEC2 const&);

zMAT3 operator*(zMAT3 const&, float);

zMAT3 operator*(float, zMAT3 const&);

zMAT3 operator/(zMAT3 const&, float);

BOOL operator==(zMAT3 const&, zMAT3 const&);

BOOL operator!=(zMAT3 const&, zMAT3 const&);



class zMAT4				
{
public:
	zVEC4		row[4];
	
public:
	zMAT4() {};
	zMAT4(zVEC4 const&, zVEC4 const&, zVEC4 const&, zVEC4 const&);
	zMAT4(float);
	zMAT4& operator+=(zMAT4 const&);
	zMAT4& operator-=(zMAT4 const&);
	zMAT4& operator*=(float);
	zMAT4& operator/=(float);
	zVEC4& operator[](int);
	const zVEC4& operator[](int) const;
	zMAT4 Transpose() const;
	zMAT4 Inverse() const;
	zMAT4 InverseLinTrafo() const;
	zMAT4& Translate(zVEC3 const&);
	zMAT4& SetTranslation(zVEC3 const&);
	zVEC3 GetTranslation() const;
	void GetTranslation(zVEC3&) const;
	void SetAtVector(zVEC3 const&);
	zVEC3 GetAtVector() const;
	void SetUpVector(zVEC3 const&);
	zVEC3 GetUpVector() const;
	void SetRightVector(zVEC3 const&);
	zVEC3 GetRightVector() const;
	zVEC3 ExtractScaling() const;
	zMAT3 ExtractRotation();
	void MakeOrthonormal();
	zVEC3 Rotate(zVEC3 const&) const;
	void PostRotateX(float);
	void PostRotateY(float);
	void PostRotateZ(float);
	void PostScale(zVEC3 const&);
	void PreScale(zVEC3 const&);
	zVEC3 GetEulerAngles() const;
	void SetByEulerAngles(zVEC3);
	BOOL IsUpper3x3Orthonormal() const;
	zMAT4& Apply(float (*)(float));
};


inline zVEC4& zMAT4::operator[](int index)
{
	return row[index];
}


inline const zVEC4& zMAT4::operator[](int index) const
{
	return row[index];
}


zMAT4 operator-(zMAT4 const&);

zMAT4 operator+(zMAT4 const&, zMAT4 const&);

zMAT4 operator-(zMAT4 const&, zMAT4 const&);

zMAT4 operator*(zMAT4 const&, zMAT4 const&);

zVEC3 operator*(zMAT4 const&, zVEC3 const&);

zMAT4 operator*(zMAT4 const&, float);

zMAT4 operator*(float, zMAT4 const&);

zMAT4 operator/(zMAT4 const&, float);

BOOL operator==(zMAT4 const&, zMAT4 const&);

BOOL operator!=(zMAT4 const&, zMAT4 const&);


class zCQuat			
{
	zVEC4		qt;
public:
	zCQuat() {};
	float& operator[](int);
	const float& operator[](int) const;
	void Unit();
	void QuatToMatrix3(zMAT3&) const;
	void QuatToMatrix4(zMAT4&) const;
	void Matrix3ToQuat(zMAT3 const&);
	void Matrix4ToQuat(zMAT4 const&);
	void QuatToEuler(zVEC3&) const;
	void EulerToQuat(zVEC3 const&);
	void QuatToAxisAngle(zVEC3&, float&) const;
	void QuatToAngAxis(zVEC3&, float&) const;
    void Slerp(float, zCQuat const&, zCQuat const&);
	void Lerp(float, zCQuat const&, zCQuat const&);
	void Squad(float, zCQuat const&, zCQuat const&, zCQuat const&, zCQuat const&);
};


inline const float& zCQuat::operator [](int nIndex) const
{
	return qt[nIndex];
}


inline float& zCQuat::operator [](int nIndex)
{
	return qt[nIndex];
}

 
inline zMAT4 Alg_Identity3D(void)
{
	XCALL(0x00517B50);
};



static float Alg_AngleUnitRad(const zVEC3& va, const zVEC3& vb)
{
	float c = va * vb;
	if(c > 1.0)
		return 0;
	if(c < -1.0)
		return (float)PI;
	return acos(c);
}



static float Alg_AngleUnit(const zVEC3& va, const zVEC3& vb)
{
	return (float)(Alg_AngleUnitRad(va, vb) * DEGREE);
}


static float Alg_AngleRad(const zVEC3& va, const zVEC3& vb)
{
	zVEC3 va2 = va;
	va2.NormalizeSafe();
	zVEC3 vb2 = vb;
	vb2.NormalizeSafe();
	return (float)Alg_AngleUnitRad(va2, vb2);
}


static float Alg_Angle(const zVEC3& va, const zVEC3& vb)
{
	return (float)(Alg_AngleRad(va, vb) * DEGREE);
}

#include "zmath.inl"