
/*
	G2OMP - CServer.cpp
	Author(s): Osmith
	Date: 18.02.19
*/

#include "stdafx.h"

CServer *g_Server = NULL;

//-------------------------------------------------------------------------------------------------------------------------

CServer::CServer() {
	Network = new CNetwork();
};

CServer::~CServer() {};

//-------------------------------------------------------------------------------------------------------------------------

void CServer::init() {
	std::cout << "# Gothic 2 Open Multiplayer server starting..." << std::endl;
	std::cout << "# Build date: " << __DATE__ << " Version: " << VERSION_SUM << std::endl;
	if (Network->init())
		std::cout << "# Server listening on " << Network->getLocalIP() << std::endl;

	loop();
}

//-------------------------------------------------------------------------------------------------------------------------

bool CServer::loop() {
	while (b_Exit == false) {
		// ��� ��� ��������

		if (b_cmdExecuted == true)		// ��������� ���� ������
			cmdHandler();

		RakSleep(1);
	}

	return 0;
}

//-------------------------------------------------------------------------------------------------------------------------

void CServer::stop() {
	b_Exit = true;
}

//-------------------------------------------------------------------------------------------------------------------------

void CServer::cmdHandler() {
	if (b_Exit == false && b_cmdExecuted == true) {
		b_cmdExecuted = false;
		std::string command;
		std::getline(std::cin, command);
		if (CommandManager->executeCmd(command))
			b_cmdExecuted = true;
	}
}