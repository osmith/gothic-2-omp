
/*
	G2OMP - GCommandManager.cpp
	Author(s): Osmith
	Date: 19.02.19
*/

#include "stdafx.h"

//-------------------------------------------------------------------------------------------------------------------------

CCommandManager::CCommandManager() {};

CCommandManager::~CCommandManager() {};

//-------------------------------------------------------------------------------------------------------------------------

bool CCommandManager::executeCmd(std::string command) {
	if (command == "-help")
		return cmd_help();
	else if (command == "-exit")
		return cmd_exit();
	else
		return cmd_help();
}

//-------------------------------------------------------------------------------------------------------------------------

bool CCommandManager::cmd_help() {
	std::cout << "# Available commands:\n# -help\n# -exit" << std::endl;
	return true;
}

bool CCommandManager::cmd_exit() {
	std::cout << "Shutting down..." << std::endl;
	g_Server->stop();
	RakSleep(1000);
	return true;
}