#pragma once

// C++ libs

#include <iostream>
#include <string>

// RakNet

#include "../Shared/RakNet/Source/MessageIdentifiers.h"
#include "../Shared/RakNet/Source/RakPeerInterface.h"
#include "../Shared/RakNet/Source/RakNetTypes.h"
#include "../Shared/RakNet/Source/RakSleep.h"

// Defines

#include "../Shared/defines.h"

// Managers

#include "CCommandManager.h"

// Server struct

#include "CNetwork.h"
#include "CServer.h"