
/*
	G2OMP - GCommandManager.h
	Author(s): Osmith
	Date: 19.02.19
*/

#pragma once

class CCommandManager {
public:
	CCommandManager();
	~CCommandManager();

	bool executeCmd(std::string command);

	// Commands //

	bool cmd_help();
	bool cmd_exit();
};